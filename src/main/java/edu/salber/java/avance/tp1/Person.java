package edu.salber.java.avance.tp1;

import java.util.Objects;

public class Person{
	private String name;
	private String firstName;
	private int age;
	
	public Person(String name, String firstName, int age) {
		super();
		this.name = name;
		this.firstName = firstName;
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	@Override
	public String toString() {
		return "Person [name=" + name + ", firstName=" + firstName + ", age=" + age + "]";
	}
	//@Override
	/*public int compareTo(Person o) {
		if(age > o.age) {
			return 1;
		}else if(age < o.age) return 1;
		else return 0;
	}*/
	@Override
	public int hashCode() {
		return Objects.hash(age, firstName, name);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		return age == other.age && Objects.equals(firstName, other.firstName) && Objects.equals(name, other.name);
	}
	
	
}
