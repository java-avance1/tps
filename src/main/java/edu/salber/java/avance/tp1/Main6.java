package edu.salber.java.avance.tp1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Main6 {
	public static void main(String[] args) {
		Person p1 = new Person("A", "A", 1);
		Person p2 = new Person("B", "B", 5);
		Person p3 = new Person("C", "C", 10);
		Person p4 = new Person("D", "D", 12);
		Person p5 = new Person("E", "E", 10);
		Person p6 = new Person("F", "F", 20);
		
		List<Person> persons = new ArrayList<Person>();
		persons.addAll(Arrays.asList(p1,p2,p3,p4,p5,p6));
		
		Comparator comparator = new Comparator<Person>(){
			@Override
			public int compare(Person o1, Person o2) {
				return o2.getAge()-o1.getAge();
			}
		};
		
		persons.sort(comparator);
		persons.forEach(System.out::println);
	}
}
