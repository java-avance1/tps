package edu.salber.java.avance.tp1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ConcurrentModificationExceptionExample {
	public static void main(String args[]) {
		List<String> sharedList = Collections.synchronizedList(new ArrayList<>(Arrays.asList("One", "Two", "Three")));

		// Thread 1
		new Thread(() -> {
			//synchronized (sharedList) {
			sharedList.forEach(item -> {
				System.out.println(item);
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}				
			});				
			//}

		}).start();

		// Thread 2
		new Thread(() -> {
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			//synchronized(sharedList) {
				sharedList.add("Four"); // Throws ConcurrentModificationException in Thread 1
			//}
			
		}).start();
	}
}
