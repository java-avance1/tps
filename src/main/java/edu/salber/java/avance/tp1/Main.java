package edu.salber.java.avance.tp1;
class SimpleThread extends Thread{

	@Override
	public void run() {
		for(int i=0; i<100;i++){
			System.out.println("OK : "+Thread.currentThread().getName()+" INTERUPTED : "+Thread.currentThread().isInterrupted());
			try {
				System.out.println("STATE 1 "+Thread.currentThread().getState());
				this.sleep(100);
				System.out.println("STATE 2 "+Thread.currentThread().getState());
			} catch (InterruptedException e) {
				e.printStackTrace();
				return;
			}
		}
		
	}
	
}

public class Main {
	public static void main(String[] args) throws InterruptedException {
		SimpleThread th = new SimpleThread();
		SimpleThread th2 = new SimpleThread();
		th.setPriority(1);
		//th.setDaemon(true);
		//th2.setDaemon(true);
		th.start();
		Thread.sleep(100);
		System.out.println(" 1 . th.getState : "+th.isInterrupted());
		//th.interrupt();
		System.out.println("2 . th.getState : "+th.isInterrupted());
		
		th2.start();
		System.out.println("FIN DU THREAD PRINCIPAL");
	}

}
