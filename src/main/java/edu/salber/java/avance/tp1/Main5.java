package edu.salber.java.avance.tp1;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class SharedResource {
	private ThreadLocal<Integer> count2 = new ThreadLocal<>();
    private int count = 0;
    private Lock lock = new ReentrantLock();
    
    public SharedResource() {
    	count2.set(0);
    }
    public   void increment() throws InterruptedException {
    	try {
        	lock.lock();
        	count++;
        	if(count2.get()!=null)
        	count2.set(count2.get()+1);
        	else count2.set(1);
    	}finally {
    		lock.unlock();
    	}

        
    }
    public  int getCount2() throws InterruptedException {
    	try {
        	lock.lock();
            return count2.get();
    	}finally {
    		lock.unlock();
    	}

    }
    public  int getCount() throws InterruptedException {
    	try {
        	lock.lock();
            return count;
    	}finally {
    		lock.unlock();
    	}

    }
}

class IncrementThread extends Thread {
    private SharedResource resource;

    public IncrementThread(SharedResource resource) {
        this.resource = resource;
    }

    public void run() {
        for (int i = 0; i < 1000; i++) {
            try {
				resource.increment();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        try {
			System.out.println("NORMAL : "+resource.getCount());
			System.out.println("THREAD LOCAL "+resource.getCount2());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}

public class Main5 {
    public static void main(String[] args) throws InterruptedException {
        SharedResource resource = new SharedResource();
        IncrementThread thread1 = new IncrementThread(resource);
        IncrementThread thread2 = new IncrementThread(resource);

        thread1.start();
        thread2.start();
        
        thread1.join();
        thread2.join();
        System.out.println("Résultat final : "+resource.getCount2());
    }
}
