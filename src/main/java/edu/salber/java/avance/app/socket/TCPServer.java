package edu.salber.java.avance.app.socket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TCPServer {
	private static  ExecutorService excutor = Executors.newFixedThreadPool(10);
	public static void main(String[] args) throws IOException, InterruptedException {
		ServerSocket server = new ServerSocket(8080);
		Socket socket = null;
		
		try {
			while(true) {
				 socket = server.accept();
				ConnexionTask task = new ConnexionTask(socket.getInputStream(), socket.getOutputStream());
				excutor.execute(task);
			}
		}finally {
			excutor.shutdownNow();
			socket.close();
			server.close();
		}

	}
}
