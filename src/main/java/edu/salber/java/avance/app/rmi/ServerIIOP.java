package edu.salber.java.avance.app.rmi;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;

public class ServerIIOP {
    public static void main(String[] args) {
        try {
            // Cr�ation de l'objet distant
            CalculService service = new CalculServiceImpl();

            // Enregistrement de l'objet distant dans l'annuaire JNDI
            Properties props = new Properties();
            props.setProperty(Context.PROVIDER_URL, "iiop://localhost:1050");
            Context ctx = new InitialContext(props);
            ctx.bind("CalculService", service);

            System.out.println("RMI server ready.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
