package edu.salber.java.avance.app.reflexion;

public interface CalculService {
    int addition(int a, int b);
    int soustraction(int a, int b);
    int multiplication(int a, int b);
    int division(int a, int b);
}