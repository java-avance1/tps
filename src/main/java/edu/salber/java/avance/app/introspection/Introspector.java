package edu.salber.java.avance.app.introspection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import lombok.Data;

public class Introspector {
	public static void printPublicMethods(Class<?> myclass) {
		Method[] methods = myclass.getMethods();
		for (Method method : methods) {
			System.out.println(method.getName());
		}
	}
	public static void printMethodsAnnotatedWith(Class<?> myclass, Class<? extends Annotation> annotation){
		Method[] methods = myclass.getMethods();
		for (Method method : methods) {
			if(method.isAnnotationPresent(annotation)) {
				System.out.println(method.getName());
			}
		}
	}
	public static void main(String[] args) {
		printPublicMethods(Person.class);
		//printMethodsAnnotatedWith(Person.class, Deprecated.class);
	}
}
