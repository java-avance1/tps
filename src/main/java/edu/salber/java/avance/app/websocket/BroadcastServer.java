package edu.salber.java.avance.app.websocket;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class BroadcastServer {
	public static void main(String[] args) throws IOException, InterruptedException {
		try (DatagramSocket socket = new DatagramSocket()) {
			String message = "192.168.10.100:8887";
			while(true) {
				 byte[] toSend =  message.getBytes();
				 InetAddress address = InetAddress.getByName("255.255.255.255");
				DatagramPacket packet = new DatagramPacket(message.getBytes(), toSend.length, address, 8081);
				socket.send(packet);
				Thread.sleep(1000*3);
			}
		}
	}
}
