package edu.salber.java.avance.app.forkjoin;

import java.util.Arrays;
import java.util.concurrent.RecursiveTask;

public class MyTask extends RecursiveTask<Long>{
	private int CHUNK = 5;
	private Long[] values;
	public MyTask(Long[] values) {
		this.values = values;
	}
	@Override
	protected Long compute() {
		if(values.length<=CHUNK) {
			return sum(this.values);
		}else {
			int lenght = this.values.length;
			int middle = lenght / 2;
			Long[] tab1 = new Long[middle];
			Long[] tab2 = new Long[lenght-middle];
			for(int i=0;i<middle;i++) {
				tab1[i] = this.values[i];
				
			}
			//System.out.println(tab1);
			for(int i=middle;i<lenght;i++) {
				tab2[i-middle] = this.values[i];
			}
			MyTask task1 = new MyTask(tab1);
			MyTask task2 = new MyTask(tab2);
			task1.fork();
			task2.fork();
			Long value1 = task1.join();
			Long value2 = task2.join();
			return value1+value2;
		}
	}

	private Long sum(Long[] subvalues) {
		return Arrays.asList(subvalues).stream().map(v -> v*v).reduce(0l, (a, b)-> a+b).longValue();
	}

}
