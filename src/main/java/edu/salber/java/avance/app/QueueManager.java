package edu.salber.java.avance.app;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class QueueManager {
	private BlockingQueue<String> queue = new LinkedBlockingQueue<>(3);

	public QueueManager(int capacity) {
		queue = new LinkedBlockingQueue<>(capacity);
	}

	public void enqueue(String item) throws InterruptedException {
		queue.put(item);
		System.out.println(item + " a �t� ajout� � la file d'attente.");
	}

	public  String dequeue() throws InterruptedException {
		 String item = queue.take();
		System.out.println(item + " a �t� retir� de la file d'attente.");	
		
		return item;
	}
	public Void maMethode() {
		return null;
		
	}
}
