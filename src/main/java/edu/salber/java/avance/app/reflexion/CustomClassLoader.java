package edu.salber.java.avance.app.reflexion;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;

public class CustomClassLoader extends ClassLoader {
    private String filePath;

    public CustomClassLoader(String filePath) {
        this.filePath = filePath;
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        try {
            byte[] classData = loadClassData();
            return defineClass(name, classData, 0, classData.length);
        } catch (IOException e) {
            throw new ClassNotFoundException("Error loading class data from file " + filePath, e);
        }
    }

    private byte[] loadClassData() throws IOException {
        File file = new File(filePath);
        try (InputStream inputStream = new FileInputStream(file)) {
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            int data;
            while ((data = inputStream.read()) != -1) {
                byteStream.write(data);
            }
            return byteStream.toByteArray();
        }
    }

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, SecurityException, InvocationTargetException {
        String filePath = "C:\\java\\TP\\wd\\java-avance\\target\\classes\\MyClass.class";
        CustomClassLoader classLoader = new CustomClassLoader(filePath);
        Class<?> clazz = classLoader.loadClass("MyClass");
        Object instance = clazz.newInstance();
        System.out.println("Loaded class: " + clazz.getName());
        instance.getClass().getMethod("lireFichier", String.class).invoke(instance, "");
    }
}
