package edu.salber.java.avance.app.reflexion;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;

public class Main {
	static Map<String, String> operations = Map.of("-", "soustraction", "+","addition", "/", "division", "*","multiplication");
	public static void main(String[] args) throws NoSuchMethodException, SecurityException, IllegalAccessException, InvocationTargetException, InstantiationException, IllegalArgumentException {
		try(Scanner scanner = new Scanner(System.in)){
			while(true) {
				System.out.println("Entrez une opération");
				String value = scanner.next();
				System.out.println(value);
				Objects.nonNull(value);
				Constructor<CalculServiceImpl> constructor = CalculServiceImpl.class.getDeclaredConstructor();
				constructor.setAccessible(true);
				CalculService instance = constructor.newInstance();
				//CalculService instance = new CalculServiceImpl();
				String operation = extract(value);
				String[] values = value.split("\\"+operation);
				Method method = CalculService.class.getMethod(operations.get(operation), int.class, int.class);
				Integer resultat = (Integer)method.invoke(instance, Integer.valueOf(values[0].trim()), Integer.valueOf(values[1].trim()));
				System.out.println("RESULTAT : "+resultat);
			}	
		}
	
	}
	private static String extract(String value) {
		return operations.keySet().stream().filter(k -> value.contains(k)).findAny().get();
	}
}
