package edu.salber.java.avance.app;

import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main2 {
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		ExecutorService service = Executors.newFixedThreadPool(10);
		Random r = new Random();
		CompletableFuture<Integer> future1 = CompletableFuture.supplyAsync(() -> ramdomRun(r), service);
		CompletableFuture<Integer> future2 = CompletableFuture.supplyAsync(() -> ramdomRun(r), service);
		future1.thenApply(result -> {
			return result * 10;
		}).thenCompose(resultat -> {
			return CompletableFuture.supplyAsync(() -> {return 1;}, service).handle((re, ex) -> {
				///ex.printStackTrace();
				return re;
			});
		});
		CompletableFuture.allOf(future1, future2).thenAccept(result -> System.out.println("FIN DES DEUX THREADS"));
		// Future<Integer> future1 = service.submit(() -> ramdomRun(r));
		// future1.get();
		System.out.println("FIn DU THRED PRINCIPAL");
		
	}

	private static int ramdomRun(Random r) {
		int resp = r.nextInt(1, 3000);
		try {
			Thread.sleep(resp);
			System.out.println("FIN DU THREAD " + Thread.currentThread().getName() + " valeur =" + resp);
			return resp;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return -1;
	}

}
