package edu.salber.java.avance.app.reflexion;

public class CalculServiceImpl  implements CalculService {
    /**
	 * 
	 */

	private CalculServiceImpl(){
        super();
    }

    public int addition(int a, int b) {
        return a + b;
    }

    public int soustraction(int a, int b) {
        return a - b;
    }

    public int multiplication(int a, int b)  {
        return a * b;
    }

    public int division(int a, int b) {
        if (b == 0) {
            throw new IllegalStateException("Division by zero");
        }
        return a / b;
    }
}
	
