package edu.salber.java.avance.app;

import java.io.FileWriter;
import java.io.IOException;

public class Consumer extends Thread{
    private QueueManager queueManager;
    private String fileName;

    public Consumer(QueueManager queueManager, String fileName) {
        this.queueManager = queueManager;
        this.fileName = fileName;
    }

    public void run() {
        try(FileWriter writer = new FileWriter(fileName, false)) {
        	 // Append mode
            while (true) {
                String message   = queueManager.dequeue();
                if("STOP".equals(message)) {
                	return;
                }
                writer.write(message);
                Thread.sleep(100);
                writer.flush();
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }
   
}
