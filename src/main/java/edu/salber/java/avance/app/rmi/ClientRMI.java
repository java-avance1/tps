package edu.salber.java.avance.app.rmi;

import java.rmi.Naming;

public class ClientRMI {
    public static void main(String[] args) {
        try {
            CalculService service = (CalculService) Naming.lookup("rmi://localhost:1099/CalculService");
            int a = 10, b = 5;
            System.out.println("Addition : " + service.addition(a, b));
            System.out.println("Soustraction : " + service.soustraction(a, b));
            System.out.println("Multiplication : " + service.multiplication(a, b));
            System.out.println("Division : " + service.division(a, b));
        } catch (Exception e) {
            System.err.println("Erreur sur le client : " + e);
        }
    }
}
