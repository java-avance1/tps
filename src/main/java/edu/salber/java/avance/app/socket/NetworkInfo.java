package edu.salber.java.avance.app.socket;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class NetworkInfo {
	private static void displayNetworkInterfaces() throws SocketException {
		Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
		while(interfaces.hasMoreElements()) {
			NetworkInterface interf = interfaces.nextElement();
			System.out.println("Interface: "+interf.getName());
			Enumeration<InetAddress> inets = interf.getInetAddresses();
			while(inets.hasMoreElements()) {
				InetAddress inet = inets.nextElement();
				System.out.println("  IP : "+inet.getHostAddress());
				System.out.println("  Name : "+inet.getHostName());
				System.out.println("  Loopback: "+interf.isLoopback());
				System.out.println("  Status: "+(interf.isUp() ? "UP" : "DOWN"));
			}
		}
	}
	public static void main(String[] args) throws SocketException {
		displayNetworkInterfaces();
	}
}
