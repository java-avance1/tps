package edu.salber.java.avance.app.jms;

import org.apache.activemq.ActiveMQConnectionFactory;

import jakarta.jms.Connection;
import jakarta.jms.ConnectionFactory;
import jakarta.jms.JMSException;
import jakarta.jms.MessageProducer;
import jakarta.jms.Session;
import jakarta.jms.TextMessage;
import jakarta.jms.Topic;

public class JMSTopicProducer {
    public static void main(String[] args) {
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
        Connection connection = null;
        try {
            // Cr�er une connexion � ActiveMQ
            connection = connectionFactory.createConnection();
            connection.start();

            // Cr�er une session JMS
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // Cr�er une destination JMS (sujet)
            Topic topic = session.createTopic("exampleTopic");

            // Cr�er un producteur de messages pour le sujet
            MessageProducer producer = session.createProducer(topic);

            // Cr�er un message texte
            TextMessage message = session.createTextMessage("Hello, JMS Topic!");

            // Envoyer le message
            producer.send(message);

            System.out.println("Message envoy� avec succ�s.");
        } catch (JMSException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
    }
}