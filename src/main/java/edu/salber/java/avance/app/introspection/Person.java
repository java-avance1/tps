package edu.salber.java.avance.app.introspection;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;


public class Person {
	@Getter
	private String name;
	@Setter @Getter
	private String firstName;
	@Setter @Getter
	private Date date;
	public void setName() {
		
	}
	@Deprecated
	public int getAge() {
		return 20;
	}
	public String getFullName() {
		return firstName+" "+name;
	}
}
