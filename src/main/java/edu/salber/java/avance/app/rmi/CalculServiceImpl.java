package edu.salber.java.avance.app.rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class CalculServiceImpl extends UnicastRemoteObject implements CalculService {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CalculServiceImpl() throws RemoteException {
        super();
    }

    public int addition(int a, int b) throws RemoteException {
        return a + b;
    }

    public int soustraction(int a, int b) throws RemoteException {
        return a - b;
    }

    public int multiplication(int a, int b) throws RemoteException {
        return a * b;
    }

    public int division(int a, int b) throws RemoteException {
        if (b == 0) {
            throw new RemoteException("Division by zero");
        }
        return a / b;
    }
}
	
