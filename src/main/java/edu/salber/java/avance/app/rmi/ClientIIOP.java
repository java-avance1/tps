package edu.salber.java.avance.app.rmi;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;

public class ClientIIOP {
	public static void main(String[] args) {
		try {
			// Configuration de l'acc�s � l'annuaire JNDI
			Properties props = new Properties();
            props.setProperty(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.cosnaming.CORBAInitialContextFactory");
			props.setProperty(Context.PROVIDER_URL, "iiop://localhost:1050");
			Context ctx = new InitialContext(props);

			// Recherche de l'objet distant dans l'annuaire JNDI
			CalculService service = (CalculService) ctx.lookup("CalculService");

			// Appel de la m�thode distante
            int a = 10, b = 5;
            System.out.println("Addition : " + service.addition(a, b));
            System.out.println("Soustraction : " + service.soustraction(a, b));
            System.out.println("Multiplication : " + service.multiplication(a, b));
            System.out.println("Division : " + service.division(a, b));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
