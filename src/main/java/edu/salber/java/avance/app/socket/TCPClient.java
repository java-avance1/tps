package edu.salber.java.avance.app.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class TCPClient {
	public static void main(String[] args) throws UnknownHostException, IOException, InterruptedException {
		Socket socket =new Socket("127.0.0.1", 8080);
		BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		PrintWriter writer = new PrintWriter(socket.getOutputStream());
		
		writer.println("COUCOU DU CLIENT");
		writer.flush();
		
		String message = reader.readLine();
		System.out.println("Message du server :"+message);

		
		Thread.sleep(100);
		
		reader.close();
		writer.close();
		socket.close();
	}
}
