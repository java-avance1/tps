package edu.salber.java.avance.app.websocket;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class BroadcastClient {
	public static void main(String[] args) throws IOException {
		try (DatagramSocket socket = new DatagramSocket(8081)) {
			byte[] receivedData = new byte[2048];
			while (true) {
				DatagramPacket packet = new DatagramPacket(receivedData, 0, receivedData.length);
				socket.receive(packet);
				String message = new String(packet.getData(), 0, packet.getLength());
				System.out.println("Message reçu : " + message);
			}
		}
	}
}
