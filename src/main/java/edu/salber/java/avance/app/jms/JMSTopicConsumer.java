package edu.salber.java.avance.app.jms;

import org.apache.activemq.ActiveMQConnectionFactory;

import jakarta.jms.Connection;
import jakarta.jms.ConnectionFactory;
import jakarta.jms.JMSException;
import jakarta.jms.Message;
import jakarta.jms.MessageConsumer;
import jakarta.jms.MessageListener;
import jakarta.jms.Session;
import jakarta.jms.TextMessage;
import jakarta.jms.Topic;

public class JMSTopicConsumer {
    public static void main(String[] args) {
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
        Connection connection = null;
        try {
            // Cr�er une connexion � ActiveMQ
            connection = connectionFactory.createConnection();
            connection.start();

            // Cr�er une session JMS
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // Cr�er une destination JMS (sujet)
            Topic topic = session.createTopic("exampleTopic");

            // Cr�er un consommateur de messages pour le sujet
            MessageConsumer consumer = session.createConsumer(topic);

            // �couter les messages
            consumer.setMessageListener(new MessageListener() {
                @Override
                public void onMessage(Message message) {
                    if (message instanceof TextMessage) {
                        try {
                            System.out.println("Message re�u du sujet: " + ((TextMessage) message).getText());
                        } catch (JMSException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            // Maintenir le programme en cours d'ex�cution
            Thread.sleep(10000);
        } catch (JMSException | InterruptedException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
    }
}
