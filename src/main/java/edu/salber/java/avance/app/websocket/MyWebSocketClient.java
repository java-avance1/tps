package edu.salber.java.avance.app.websocket;

import java.net.URI;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

public class MyWebSocketClient extends WebSocketClient {
    public MyWebSocketClient(URI serverUri) {
        super(serverUri);
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {
        System.out.println("Connexion �tablie avec le serveur : " + getURI());
        this.send("Bonjour");
    }

    @Override
    public void onMessage(String message) {
        System.out.println("Message re�u du serveur : " + message);
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        System.out.println("Connexion avec le serveur ferm�e.");
    }

    @Override
    public void onError(Exception ex) {
        System.err.println("Une erreur s'est produite : ");
        ex.printStackTrace();
    }

}
