package edu.salber.java.avance.app.websocket;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;

public class MyWebSocketServer extends WebSocketServer {
    public MyWebSocketServer(InetSocketAddress address) {
        super(address);
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        System.out.println("Nouvelle connexion : " + conn.getRemoteSocketAddress());
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        System.out.println("Connexion ferm�e : " + conn.getRemoteSocketAddress());
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        System.out.println("Message re�u de " + conn.getRemoteSocketAddress() + ": " + message);
        conn.send("Message re�u : " + message); // Envoyer un message en r�ponse
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        System.err.println("Une erreur s'est produite : ");
        ex.printStackTrace();
    }

	@Override
	public void onStart() {
		System.out.println("Demarr�e");
	}


}