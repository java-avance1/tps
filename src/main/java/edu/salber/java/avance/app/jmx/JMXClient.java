package edu.salber.java.avance.app.jmx;

import java.lang.management.ThreadMXBean;

import javax.management.JMX;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

public class JMXClient {

	public static void main(String[] args) throws Exception {
		JMXServiceURL url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://localhost:9999/jmxrmi");
		try (JMXConnector jmxc = JMXConnectorFactory.connect(url)) {
			MBeanServerConnection mbsc = jmxc.getMBeanServerConnection();
			// ObjectName mbeanName = new
			// ObjectName("edu.salber.java.avance.app.jmx:type=PerformanceMonitor");
			mbsc.queryNames(null, null);
			ObjectName mbeanName = new ObjectName("java.lang:type=Threading");
			ThreadMXBean mbeanProxy = JMX.newMBeanProxy(mbsc, mbeanName, ThreadMXBean.class);
			System.out.println("Uptime: " +mbeanProxy.getThreadCount()
					+ " milliseconds");
		}
	}
}
