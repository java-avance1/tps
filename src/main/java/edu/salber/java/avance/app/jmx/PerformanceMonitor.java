package edu.salber.java.avance.app.jmx;

import java.lang.management.ManagementFactory;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Set;

import javax.management.MBeanServer;
import javax.management.ObjectName;

public class PerformanceMonitor implements PerformanceMonitorMBean {

    private long startTime;

    public PerformanceMonitor() {
        this.startTime = System.currentTimeMillis();
    }

    @Override
    public long getUptime() {
        return System.currentTimeMillis() - startTime;
    }
    /** A lancer avec les parametre JVM suivants (a partir du jdk 18)
     * -Dcom.sun.management.jmxremote
	* -Dcom.sun.management.jmxremote.port=9999
	* -Dcom.sun.management.jmxremote.authenticate=false
	* -Dcom.sun.management.jmxremote.ssl=false
     * 
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
        ObjectName name = new ObjectName("edu.salber.java.avance.app.jmx:type=PerformanceMonitor");
        PerformanceMonitor mbean = new PerformanceMonitor();
        mbs.registerMBean(mbean, name);
        System.out.println("MBean enregistr� avec succ�s !");
        Thread.sleep(Long.MAX_VALUE); // Attend ind�finiment
    }
}
