package edu.salber.java.avance.app;

import java.util.concurrent.Callable;

public class ProducerTask implements Callable<Boolean>{
    private QueueManager queueManager;
    private int lineNum;
	public ProducerTask(QueueManager queueManager, int lineNum) {
        this.queueManager = queueManager;
        this.lineNum = lineNum;		
	}
	@Override
    public Boolean call() {
        try {
            for (int i = 0; i < lineNum; i++) {
                queueManager.enqueue("Ligne "+(i+1)+" from thread "+Thread.currentThread().getName()+"\n");
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return true;
    }

}
