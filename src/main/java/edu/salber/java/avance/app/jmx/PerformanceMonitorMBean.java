package edu.salber.java.avance.app.jmx;

public interface PerformanceMonitorMBean {
	long getUptime();
}
