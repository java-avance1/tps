package edu.salber.java.avance.app.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CalculService extends Remote {
    int addition(int a, int b) throws RemoteException;
    int soustraction(int a, int b) throws RemoteException;
    int multiplication(int a, int b) throws RemoteException;
    int division(int a, int b) throws RemoteException;
}