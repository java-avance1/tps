package edu.salber.java.avance.app.websocket;

import java.net.InetSocketAddress;

public class ServerMain {
	public static void main(String[] args) {
        MyWebSocketServer server = new MyWebSocketServer(new InetSocketAddress(8887));
        server.start();
        System.out.println("Serveur WebSocket d�marr� sur le port 8887");
    }
}
