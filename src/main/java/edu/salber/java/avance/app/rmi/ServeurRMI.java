package edu.salber.java.avance.app.rmi;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

public class ServeurRMI {
    public static void main(String[] args) {
        try {
            CalculService service = new CalculServiceImpl();
            LocateRegistry.createRegistry(1099); // Cr�er un registre RMI sur le port 1099
            Naming.rebind("CalculService", service); // Lier l'impl�mentation au nom "CalculService"
            System.out.println("Serveur RMI pr�t.");
        } catch (Exception e) {
            System.err.println("Erreur sur le serveur : " + e);
        }
    }
}