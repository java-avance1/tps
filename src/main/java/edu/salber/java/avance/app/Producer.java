package edu.salber.java.avance.app;

public class Producer extends Thread{
    private QueueManager queueManager;
    private int lineNum;

    public Producer(QueueManager queueManager, int lineNum) {
        this.queueManager = queueManager;
        this.lineNum = lineNum;
    }

    public void run() {
        try {
            for (int i = 0; i < lineNum; i++) {
                //queueManager.enqueue("Ligne "+(i+1)+" from thread "+Thread.currentThread().getName()+"\n");
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
