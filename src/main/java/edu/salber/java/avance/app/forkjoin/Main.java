package edu.salber.java.avance.app.forkjoin;

public class Main {
	public static void main(String[] args) {
		int a = 2;
		int b = 2000;
		Long[] values = new Long[b-a+1];
		for(int i=a;i<=b;i++) {
			values[i-a] = Long.valueOf(i); 
		}
		MyTask task = new MyTask(values);
		Long result = task.invoke();
		System.out.println("RESULTAT : "+result);
	}
}
