package edu.salber.java.avance.app;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class Main {
	private static int NUB_PRODUCERS = 10;
	private static int NUM_LINE = 10;
	private static String FILE_NAME= "./data1.txt";
	
	private static ExecutorService service = Executors.newFixedThreadPool(2);
	
	public static void main(String[] args) throws InterruptedException, IOException {
        QueueManager queueManager = new QueueManager(3); // Capacit� de la file d'attente : 3
        Consumer consumer = new Consumer(queueManager, "./data1.txt");
        consumer.start();
        List<Future<Boolean>> listThreads = new ArrayList<>();
        for(int i=0;i<NUB_PRODUCERS;i++) {
        	ProducerTask task = new ProducerTask(queueManager, NUM_LINE);
        	Future<Boolean> future = service.submit(task);
        	listThreads.add(future);
        }
        service.shutdown();
        
        if(!service.awaitTermination(5, TimeUnit.MINUTES)) {
        	service.shutdownNow();
        }
      queueManager.enqueue("STOP");
      consumer.join();
        try (Stream<String> fileStream = Files.lines(Paths.get(FILE_NAME))) {
    		//Lines count
    		int noOfLines = (int) fileStream.count();
    		System.out.println("NOMBRE TOTAL DE LIGNE : "+noOfLines);
    	}        
	}
}
