package edu.salber.java.avance.app.jms;


import org.apache.activemq.ActiveMQConnectionFactory;

import jakarta.jms.Connection;
import jakarta.jms.ConnectionFactory;
import jakarta.jms.JMSException;
import jakarta.jms.MessageProducer;
import jakarta.jms.Queue;
import jakarta.jms.Session;
import jakarta.jms.TextMessage;


public class JMSProducer {
    public static void main(String[] args) {
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
        Connection connection = null;
        try {
            // Cr�er une connexion � ActiveMQ
            connection = connectionFactory.createConnection();
            connection.start();

            // Cr�er une session JMS
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // Cr�er une destination JMS (file d'attente)
            Queue destination = session.createQueue("exampleQueue");

            // Cr�er un producteur de messages pour la destination
            MessageProducer producer = session.createProducer(destination);

            // Cr�er un message texte
            TextMessage message = session.createTextMessage("Hello, JMS!");

            // Envoyer le message
            producer.send(message);

            System.out.println("Message envoy� avec succ�s.");
        } catch (JMSException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
    }
}
