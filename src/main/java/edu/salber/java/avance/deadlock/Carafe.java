package edu.salber.java.avance.deadlock;

public class Carafe {

	// permet de savoir si la carafe est vide ou pas
	private boolean plein = false;

	// un verre a besoin d'un verre pour se vider
	private Verre verre;

	// on ne peut se remplir que si le verre est plein
	public synchronized void remplis() {
		if (verre.isPlein()) {
			plein = true;
			verre.setPlein(false);
		}
	}

	// et l'on ne peut se vider que si le verre est vide
	public synchronized void vide() {
		if (!verre.isPlein()) {
			plein = false;
			verre.setPlein(true);
		}
	}

	// permet au monde extérieur de sa voir
	// si nous sommes plein ou vide
	public synchronized boolean isPlein() {
		return this.plein;
	}

	// reste des getters non synchronisé
	public void setPlein(boolean plein) {
		this.plein = plein;
	}
	public synchronized void setVerre(Verre verre) {
		this.verre = verre;
	}
	public Verre getVerre() {
		return verre;
	}
}
