package edu.salber.java.avance.deadlock;

public class Main {
	public static void main(String[] args) throws InterruptedException {
		// dans une m�thode main
		// initialisation d'une carafe et d'un verre, associ�s l'un � l'autre
		final Carafe carafe = new Carafe();
		final Verre verre = new Verre();
		carafe.setVerre(verre);
		verre.setCarafe(carafe);

		// cr�ation d'une instance de Runnable qui remplit et vide la carafe
		// dans le verre
		// on peut augmenter le nombre de cycles dans la boucle for
		Runnable carafeApp = new Runnable() {

			public void run() {

				for (int i = 0; i < 20; i++) {
					carafe.remplis();
					carafe.vide();
				}
			}
		};

		// cr�ation d'une instance de Runnable qui remplit et vide le verre
		// dans la carafe
		// on peut augmenter le nombre de cycles dans la boucle for
		Runnable verreApp = new Runnable() {

			public void run() {

				for (int i = 0; i < 20; i++) {
					verre.remplis();
					verre.vide();
				}
			}
		};

		// cr�ation des threads
		Thread[] carafeApps = new Thread[5];
		Thread[] verreApps = new Thread[5];

		// creation des threads pour le vidage de la carafe
		// et du verre
		for (int i = 0; i < carafeApps.length; i++) {
			carafeApps[i] = new Thread(carafeApp);
			verreApps[i] = new Thread(verreApp);
		}

		// lancement des threads
		for (int i = 0; i < carafeApps.length; i++) {
			carafeApps[i].start();
			verreApps[i].start();
		}

		// attente de la fin de l'ex�cution des threads
		for (int i = 0; i < carafeApps.length; i++) {
			carafeApps[i].join();
			verreApps[i].join();
		}
	}
}
