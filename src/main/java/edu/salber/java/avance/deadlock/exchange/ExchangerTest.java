package edu.salber.java.avance.deadlock.exchange;

import java.util.concurrent.Exchanger;

class Producer extends Thread {
    private Exchanger<String> exchanger;

    public Producer(Exchanger<String> exchanger) {
        this.exchanger = exchanger;
    }

    public void run() {
        try {
            String data = "Produit par le producteur";
            System.out.println("Producteur �change : " + data);
            Thread.sleep(1000); // Simule un travail
            String exchangedData = exchanger.exchange(data);
            System.out.println("Producteur a re�u : " + exchangedData);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class Consumer extends Thread {
    private Exchanger<String> exchanger;

    public Consumer(Exchanger<String> exchanger) {
        this.exchanger = exchanger;
    }

    public void run() {
        try {
            String data = "Consomm� par le consommateur";
            System.out.println("Consommateur �change : " + data);
            Thread.sleep(2000); // Simule un travail
            String exchangedData = exchanger.exchange(data);
            System.out.println("Consommateur a re�u : " + exchangedData);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

public class ExchangerTest {
    public static void main(String[] args) {
        Exchanger<String> exchanger = new Exchanger<>();

        Producer producer = new Producer(exchanger);
        Consumer consumer = new Consumer(exchanger);

        producer.start();
        consumer.start();
    }
}