package edu.salber.java.avance.deadlock.exchange;

import java.util.concurrent.CyclicBarrier;

class Worker extends Thread {
    private CyclicBarrier barrier;

    public Worker(CyclicBarrier barrier) {
        this.barrier = barrier;
    }

    public void run() {
        try {
            System.out.println(Thread.currentThread().getName() + " attend...");
            barrier.await(); // Attend que tous les threads aient atteint le point de barri�re
            System.out.println(Thread.currentThread().getName() + " a pass� la barri�re.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

public class CyclicBarrierTest {
    public static void main(String[] args) {
        final int THREAD_COUNT = 3;
        CyclicBarrier barrier = new CyclicBarrier(THREAD_COUNT, () ->{ System.out.println("Les theads sont arrivés à la barrière.");});

        for (int i = 0; i < THREAD_COUNT; i++) {
            Worker worker = new Worker(barrier);
            worker.start();
        }
        System.out.println("Fin du thread principal");
    }
}
