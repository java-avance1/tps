package edu.salber.java.avance.deadlock;

public  class Verre {

    // permet de savoir si le verre est vide ou pas
    private  boolean plein = false ;
   
    // un verre a besoin d'une carafe pour se remplir
    private Carafe carafe ;

    // on ne peut se remplir que si la carafe est pleine
    public  synchronized  void remplis() {
       if (carafe.isPlein()) {
         plein = true ;
         carafe.setPlein(false) ;
      }
   }

    // et l'on ne peut se vider que si la carafe est vide
    public  synchronized  void vide() {
       if (!carafe.isPlein()) {
         plein = false ;
         carafe.setPlein(true) ;
      }
   }
   
    // permet au monde extérieur de sa voir
    // si nous sommes plein ou vide
    public  synchronized  boolean isPlein() {
       return  this.plein ;
   }

    // reste des getters non synchronisé
    public  void setPlein(boolean plein) {
       this.plein = plein ;
   }
    public synchronized void setCarafe(Carafe carafe) {
        this.carafe = carafe ;
    }
    public Carafe getCarafe() {
       return carafe ;
   }
}